
import json

# 检查注释文件内容
annotation_file = r'D:\Desktop\s\annotations\instances_train2017.json'  # 替换为你的标注文件路径
with open(annotation_file, 'r') as file:
    annotations = json.load(file)
print(type(annotations))  # 打印注释文件的类型