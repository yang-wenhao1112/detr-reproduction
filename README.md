# DETR复现

#### 介绍
复现了 Facebook AI 团队在2020年发表的论文《 End to End Object Detection with Transformers》，简称DETR模型，官方源码只提供训练评估源码，在此基础上我加入了预测代码，现完整代码已跑通，开源使用，仅供学习。

#### 官方源码
https://github.com/facebookresearch/detr


#### subset.py

用于下采样coco数据集,因为coco数据集太大，所以自己写了个下采样的脚本

#### experment.py

用于调试项目（无关重要），我自己调试用的

#### modify_weight.py

用于下载resnet50之后，改一下自己的类别参数，生成自己的权重文件.pth


#### Predict.py

用于预测的文件（官网并未提供），自己写的，可直接用，修改一下里面路径即可

#### panopticapi-master和cocoapi-master

这两个是加载coco数据集的库，一般不好装，这里给出了安装包，下载后在解释器里面cd到PythonAPI的路径，然后命令行输入 python setup.py
运行，即可装好，其他的包都好装，但是注意opencv和numpy两个库的兼容性

#### 说明

1.首先下载好官方的源码，在pycharm里打开
2.配置自己的环境，包括GPU,库等
3.下载coco数据集，下载完成之后可以下采样coco数据集用于训练，或者用labelme制作自己的数据集
4.开始训练，训练完成之后会在output生成自己的训练模型
5.将predict.py文件放入主文件里，调整自己的coco数据集路径，预测文件路径，保存路径，模型路径等
6.万事大吉

#### CSDN博客

https://mp.csdn.net/mp_blog/creation/editor/134451939

#### bilibili运行视频

https://www.bilibili.com/video/BV1xH4y1q7S2/?vd_source=b4a44218b2427b94e405410c3ee310dc

